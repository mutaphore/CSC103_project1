//CPE103 - Gharibyan - Project 1
//Andy Chen & Michael Woodson
//ConTest
//Client class that requests the user to perform infix and postfix expressions
//then do operations to convert and evaluate them.
import java.util.*;

public class ConTest {
   
   public static void main(String[] args){
      
      Scanner input = new Scanner(System.in);
      char choice = 'z';

      System.out.println("Choose one of the following operations:");
      System.out.println("- Infix to postfix conversion (enter the letter i)");
      System.out.println("- Postfix expression evaluation (enter the letter p),");
      System.out.println("- Arithmetic expression evaluation (enter the letter a)");
      System.out.println("- Quit the program (enter the letter q)");
      
      while(choice != 'q')
      {
         System.out.print("Enter choice: ");
         choice = input.nextLine().charAt(0);
         
         switch(choice){
            case 'i':
               System.out.print("Enter infix expression: ");
               System.out.println("The postfix expression is: " + Converter.infixToPostfix(input.nextLine()));
               break;
            case 'p':
               System.out.print("Enter postfix expression: ");
               System.out.println("The postfix evaluation is: " + Converter.postfixValue(input.nextLine()));
               break;
            case 'a':
               System.out.print("Enter infix expresion: ");
               System.out.println("The value of the arithmetic expression is: " + Converter.postfixValue(Converter.infixToPostfix(input.nextLine())));
               break;
            case 'q':
               System.out.print("Farewell!\n");
               break;
            default:
               System.out.print("Invalid choice\n");
               break;
         }
         
      }
      
      
   }
   
}
