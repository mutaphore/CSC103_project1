//CPE103 - Gharibyan - Project 1
//Andy Chen & Michael Woodson
//StackTest
//A client class that checks if MyStack Class is working properly.
import java.util.*;

public class StackTest {
   
   public static void main(String[] args){
      
      MyStack<String> stack = new MyStack<String>();
      Scanner input = new Scanner(System.in);
      char choice = 'z';
      String temp;
      
      System.out.println("Choose one of the following operations:");
      System.out.println("- push/add (enter the letter a)");
      System.out.println("- pop/delete (enter the letter d)");
      System.out.println("- peek (enter the letter p)");
      System.out.println("- check if the list is empty (enter the letter e)");
      System.out.println("- quit (enter the letter q)");
	
      while(choice != 'q')
      {
         System.out.println("Enter choice: ");
         choice = input.nextLine().charAt(0);
   
         switch (choice)
         {
            case 'a':
               System.out.println("Please enter string to be added to stack: ");
               if(input.hasNext())
               {
                  temp = input.nextLine();
                  stack.push(temp);
                  System.out.println(temp + " pushed in");
               }
               else
               {
                  System.out.println("Invalid !");
                  input.nextLine();
               }
               break;
            case 'd':
               try
               {
                  temp = stack.pop();
                  System.out.println(temp + " popped out");
               }
               catch (EmptyStackException e)
               {
                  System.out.println("Invalid operation on an empty stack");
               }
               break;
            case 'e':
               if(stack.isEmpty())
                  System.out.println("empty");
               else
                  System.out.println("not empty");
               break;
            case 'p':
               try
               {
                  temp = stack.peek();
                  System.out.println(temp + " on the top");
               }
               catch (EmptyStackException e)
               {
                  System.out.println("Invalid operation on an empty stack");
               }
            break;
            case 'q':
                  System.out.println("Quitting");
               break;
            default:
               System.out.println("Invalid choice");
               break;     
         }
      }
      
      while(!stack.isEmpty())
      {
         System.out.print(stack.pop() + " ");
      }
   
   }
   
}
