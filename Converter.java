//CPE103 - Gharibyan - Project 1
//Andy Chen & Michael Woodson
//Converter
//Contains methods to convert inflix to postfix expressions and evaluating
//the value of postfix expression
import java.util.*;

public class Converter {
   
   //Method compareOps:
   //Sets the precedence values for operators and determine which operation goes first
   //Pass 2 strings: op1 and op2, representing the operators to be compared
   //Returns TRUE if op1 has more priority than op2, else return FALSE
   private static boolean compareOps(String op1, String op2){
      
      int op1num = 1;
      int op2num = 2;
      
      if(op1.equals("+") || op1.equals("-"))
         op1num = 1;
      else if(op1.equals("*") || op1.equals("/"))
         op1num = 2;
      else if(op1.equals("("))
         op1num = 3;
      
      if(op2.equals("+") || op2.equals("-"))
         op2num = 1;
      else if(op2.equals("*") || op2.equals("/"))
         op2num = 2;
      else if(op2.equals("("))
         op2num = 3;
      
      return op1num >= op2num;
        
   }
   
   //Method infixToPostfix:
   //Convert infix expression to postfix expression
   //Pass the infix expression as String, then return the postfix expression as String
   public static String infixToPostfix (String expression){
      
      MyStack<String> stack = new MyStack<String>();
      String convertedExp = "";
      String temp;
      Scanner scan = new Scanner(expression);
      
      while(scan.hasNext()){
         
         temp = scan.next();
                  
         if(!temp.equals("+") && !temp.equals("-") && !temp.equals("/") &&
               !temp.equals("*") && !temp.equals("(") && !temp.equals(")")){
            convertedExp = convertedExp.concat(temp + " ");
         }
         else{
            if(!stack.isEmpty()){
               if(temp.equals(")")){
                  while(!stack.isEmpty() && !stack.peek().equals("(")){
                     convertedExp = convertedExp.concat(stack.pop() + " ");
                  }
                  stack.pop();
               }
               else if(!stack.peek().equals("(")){
                  while(!stack.isEmpty() && compareOps(stack.peek(),temp) && 
                        !stack.peek().equals("(")){
                     convertedExp = convertedExp.concat(stack.pop() + " ");
                  }
                  stack.push(temp);
               }
               else
                  stack.push(temp);
            }
            else
               stack.push(temp);
         }

      }
      
      while(!stack.isEmpty()){
         convertedExp = convertedExp.concat(stack.pop() + " ");
      }
      
      return convertedExp;
      
   }
   
   //Method postfixValue:
   //Evaluates a postfix expression and determine its numeric value
   //Pass the postfix expression as a String, returns numeric value as a double
   public static double postfixValue (String expression){

      MyStack<Double> stack = new MyStack<Double>();
      Scanner scan = new Scanner(expression);
      String temp = "";
      double result = 0;
      double temp1 = 0;
      double temp2 = 0;
      
      while(scan.hasNext()){
         temp = scan.next();
         
         if(!temp.equals("+")  && !temp.equals("-") && !temp.equals("/") &&
               !temp.equals("*")){
            stack.push(Double.parseDouble(temp));
         }
         else{
            
            temp1 = stack.pop();
            temp2 = stack.pop();
            
            if(temp.equals("+"))
               result = temp2 + temp1;
            else if(temp.equals("-"))
               result = temp2 - temp1;
            else if(temp.equals("/"))
               result = temp2 / temp1;
            else
               result = temp2 * temp1;
            
            stack.push(result);
         }
           
      }
      
      return stack.pop();
      
   }
   
}
